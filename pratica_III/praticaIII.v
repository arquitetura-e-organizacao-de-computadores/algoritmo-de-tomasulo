`define TAG_EMPTY 11'b11111111111
//Operacoes
`define ADD 4'b0000
`define SUB 4'b0001
`define MUL 4'b0010
`define DIV 4'b0011
`define BEQ 4'b0100
`define LD 4'b0101
`define ST 4'b0111

//Registradores
`define R0 4'b0000
`define R1 4'b0001
`define R2 4'b0010
`define R3 4'b0011
`define R4 4'b0100
`define R5 4'b0101
`define R6 4'b0110
`define R7 4'b0111


module praticaIII(
	input [17:0] SW,
	output [0:6] HEX0, output [0:6] HEX1, output [0:6] HEX2, output [0:6] HEX3, output [0:6] HEX4, output [0:6] HEX5, output [0:6] HEX6, output [0:6] HEX7,
	output [17:0] LEDR,
	output [8:0] LEDG
);	
	reg clock, run, reset;
	
	reg done;
	reg [15:0] instruction_memory [0:63];
	reg [7:0] number_of_instructions_executed, pc;
	reg [15:0] clock_counter;
	
	//Register Bank
		reg [15:0] register_bank_data [0:15]; 	//Armazena os dados do registrador
		reg [10:0] register_bank_tag [0:15];	//Armazena a tag do registrador
		
	//Add Reservation Station
		reg [3:0] reservation_station_add_op [0:3]; 		//Define a operacao
		reg [10:0] reservation_station_add_tag [0:3];	//Define a tag
		reg reservation_station_add_busy [0:3]; 			//Define se a linha e valida
		reg [15:0] reservation_station_add_Vj [0:3];
		reg [10:0] reservation_station_add_Qj [0:3];
		reg [15:0] reservation_station_add_Vk [0:3];
		reg [10:0] reservation_station_add_Qk [0:3];	
		reg [3:0]  reservation_station_add_slots;
	//Mult Reservation Station
		reg [3:0] reservation_station_mul_op [0:3];		//Define a operacao
		reg [10:0] reservation_station_mul_tag [0:3];	//Define tag
		reg reservation_station_mul_busy [0:3];			//Define se a linha e valida
		reg [15:0] reservation_station_mul_Vj [0:3];
		reg [10:0] reservation_station_mul_Qj [0:3];
		reg [15:0] reservation_station_mul_Vk [0:3];
		reg [10:0] reservation_station_mul_Qk [0:3];
		reg [3:0]  reservation_station_mul_slots;
	//Ld Reservation Station
		reg reservation_station_ld_busy [0:3];				//Define se a linha e valida
		reg [10:0] reservation_station_ld_tag [0:3];	//Define tag
		reg [15:0] reservation_station_ld_Vj [0:3];
		reg [10:0] reservation_station_ld_Qj [0:3];
		reg [7:0] reservation_station_ld_offset [0:3];	//Define o offset	
		reg [3:0] reservation_station_ld_slots;
	//St Reservation Station
		reg reservation_station_st_busy [0:3];				//Define se a linha e valida
		reg [10:0] reservation_station_st_tag [0:3];	//Define tag
		reg [15:0] reservation_station_st_Vj [0:3];
		reg [10:0] reservation_station_st_Qj [0:3];
		reg [3:0] reservation_station_st_offset [0:3];	//Define o offset
		reg [15:0] reservation_station_st_Vk [0:3];
		reg [10:0] reservation_station_st_Qk [0:3];
		reg [3:0] reservation_station_st_slots;
	
	reg CDB_busy;

	//ADD Unit
		reg unit_add_busy, unit_add_done;
		reg [15:0] unit_add_A, unit_add_B, unit_add_C;	//Define os operandos
		reg [1:0] unit_add_state;					//Define o estado da operacao
		reg [0:3] unit_add_index;					//Define o index da operacao na estacao de reserva
		reg [3:0] unit_add_op;						//Define o tipo de operacao
		reg [7:0] branch;
	//MUL Unit
		reg unit_mul_busy, unit_mul_done;
		reg [15:0] unit_mul_A, unit_mul_B, unit_mul_C;	//Define os operandos
		reg [3:0] unit_mul_state;					//Define o estado da operacao
		reg [0:3] unit_mul_index;					//Define o index da operacao na estacao de reserva
		reg [3:0] unit_mul_op;						//Define o tipo de operacao
	//ADDR Unit
		reg unit_addr_busy, unit_addr_done;
		reg [10:0] unit_addr_tag;				//Define a tag da instrucao
		reg [15:0] unit_addr_A, unit_addr_B, unit_addr_C;//Define os operandos
		reg [0:3] unit_addr_index;					//Define o index da operacao na estacao de reserva
		reg [1:0] unit_addr_state;					//Define o estado da operacao
		reg [3:0] unit_addr_op;					//Define o tipo de instrucao

	//MEM Unit
		reg mem_busy;
		reg [15:0] memory_data [63:0];	//Memoria de dados		
	
	//Reorder Buffer
		reg [7:0] reorder_buffer_index; 			//Define do index do ROB
		reg [7:0] reorder_buffer_busy;			//Define se a linha e valida
		reg [3:0] reorder_buffer_op [0:7];  	//Define a operacao da instrucao do ROB
		reg [3:0] reorder_buffer_dst [0:7];  	//Define o registrador de destino da instrucao do ROB
		reg [10:0] reorder_buffer_tag [0:7]; //Define a tag da instrucao do ROB que tambem serve para indicar o (pc-1) se o desvio n for tomado
		reg [15:0] reorder_buffer_value [0:7]; //Define o valor da instrucao do ROB se for o mesmo da tag o desvio foi tomado
		reg [7:0] reorder_buffer_have_value;	//Define se ha um valor no buffer de ordenacao
		reg [7:0] reorder_buffer_pc [0:7];
		
	reg [3:0]instruction_0, instruction_0_A, instruction_0_B, instruction_0_C;
	reg [3:0]instruction_1, instruction_1_A, instruction_1_B, instruction_1_C;
	
	reg [7:0] reorder_buffer_slots;
	
	integer i, j, k;
	
	reg BREAK;
	always @(posedge clock & run) begin
		if(reset) 
			pc = 8'b0;
		if(reorder_buffer_index == 8)
			reorder_buffer_index = 0;
			
		reorder_buffer_slots = 8'b0;
		//Se a unidade de reordenacao de instrucoes estiver vazia entao soma +1 aos seus slots
		for(i=0; i<8; i=i+1)
			if(!reorder_buffer_busy[i]) reorder_buffer_slots = reorder_buffer_slots + 8'b1;
				
		reservation_station_add_slots = 4'b0;
		reservation_station_mul_slots = 4'b0;
		reservation_station_ld_slots = 4'b0;
		reservation_station_st_slots = 4'b0;
		for(i=0; i<4; i=i+1) begin
			if(!reservation_station_add_busy[i]) reservation_station_add_slots = reservation_station_add_slots + 4'b1;
			if(!reservation_station_mul_busy[i]) reservation_station_mul_slots = reservation_station_mul_slots + 4'b1;
			if(!reservation_station_ld_busy[i]) reservation_station_ld_slots = reservation_station_ld_slots + 4'b1;
			if(!reservation_station_st_busy[i]) reservation_station_st_slots = reservation_station_st_slots + 4'b1;
		end
		
		if(reorder_buffer_index > 8) begin
			reorder_buffer_index = 1;
		end
		//Se nao existe uma estacao de reserve cheia
		if(reservation_station_add_slots > 1 & reservation_station_mul_slots > 1 & reservation_station_ld_slots > 1 & reservation_station_st_slots > 1) begin
			//Se ainda ha espaco no buffer de reordenacao e ainda nao foram executadas todas as instrucoes
			if(reorder_buffer_slots > 8'b0 & pc < number_of_instructions_executed) begin
				instruction_0 = instruction_memory[pc][15:12];
						instruction_0_A = instruction_memory[pc][11:8];
						instruction_0_B = instruction_memory[pc][7:4];
						instruction_0_C = instruction_memory[pc][3:0];					
						
				//Se as instrucoes sao de ADD, SUB, BEQ
				if(instruction_0 == `ADD | instruction_0 == `SUB | instruction_0 == `BEQ) begin
					BREAK = 1;
					for(i=0; i<4; i=i+1)
						if(BREAK & !reservation_station_add_busy[i]) begin
							reservation_station_add_busy[i] = 1'b1;
							reservation_station_add_op[i] = instruction_0;
							reservation_station_add_tag[i] = {clock_counter[4:0],pc[5:0]};
							
							populates_the_i_reservation_station_if_there_is_data_dependency(
								reservation_station_add_Vj[i], reservation_station_add_Qj[i], reservation_station_add_Vk[i], reservation_station_add_Qk[i],
								instruction_0, instruction_0_A, instruction_0_B, instruction_0_C
							);						
							if(instruction_0 != `BEQ) //Se a instrucao nao for de desvio preenche com a tag
								register_bank_tag[instruction_0_A] = {clock_counter[4:0],pc[5:0]};
							
							if(instruction_0 == `BEQ) begin
								//reservation_station_add_op[i] = `BEQ;
								pc = instruction_0_A;
							end
							
							BREAK = 1'b0;
						end
				end
				else if(instruction_0 == `MUL | instruction_0 == `DIV) begin
					BREAK = 1'b1;
					for(i=0; i<4; i=i+1)
						if(BREAK & !reservation_station_mul_busy[i]) begin
							reservation_station_mul_busy[i] = 1'b1;
							reservation_station_mul_op[i] = instruction_0;
							reservation_station_mul_tag[i] = {clock_counter[4:0],pc[5:0]};
							
							populates_the_i_reservation_station_if_there_is_data_dependency(
								reservation_station_mul_Vj[i], reservation_station_mul_Qj[i], reservation_station_mul_Vk[i], reservation_station_mul_Qk[i],
								instruction_0, instruction_0_A, instruction_0_B, instruction_0_C
							);
							//Define a tag para o registrador de destino
							register_bank_tag[instruction_0_A] = {clock_counter[4:0],pc[5:0]};
							
							BREAK = 1'b0;
						end				
				end
				else if(instruction_0 == `LD) begin
					BREAK = 1;
					for(i=0; i<4; i=i+1)
						if(BREAK & !reservation_station_ld_busy[i]) begin
							reservation_station_ld_busy[i] = 1'b1;
							reservation_station_ld_tag[i] = {clock_counter[4:0],pc[5:0]};
							reservation_station_ld_offset[i] = instruction_0_B;
							
							if(register_bank_tag[instruction_0_C] != `TAG_EMPTY)
								reservation_station_ld_Qj[i] = register_bank_tag[instruction_0_C];
							else begin
								reservation_station_ld_Qj[i] = `TAG_EMPTY;
								reservation_station_ld_Vj[i] = register_bank_data[instruction_0_C];
							end
							//Define a tag para o registrador de destino
							register_bank_tag[instruction_0_A] = {clock_counter[4:0],pc[5:0]};
							
							BREAK = 1'b1;
							k = reorder_buffer_index;			
							while(reorder_buffer_busy[k])
								k = k + 1;
							if(k == 8) k = 0;
							for(j = k; j<8; j=j+1)
								if(BREAK & !reorder_buffer_busy[j]) begin
									reorder_buffer_busy[j] = 1'b1;
									reorder_buffer_op[j] = instruction_0;  //Define aoperacao
									reorder_buffer_have_value[j] = 1'b0;
									reorder_buffer_tag[j] = {clock_counter[4:0],pc[5:0]};
									reorder_buffer_dst[j] = instruction_0_A;
									BREAK = 1'b0;
								end						
							BREAK = 1'b0;
						end                
				end
				else if(instruction_0 == `ST) begin
					BREAK = 1'b1;
					for(i=0; i<4; i=i+1)
						if(BREAK & !reservation_station_st_busy[i]) begin
							reservation_station_st_busy[i] = 1'b1;
							reservation_station_st_tag[i] = {clock_counter[4:0],pc[5:0]};
							reservation_station_st_offset[i] = instruction_0_B;
							
							populates_the_i_reservation_station_if_there_is_data_dependency(
								reservation_station_st_Vj[i], reservation_station_st_Qj[i], reservation_station_st_Vk[i], reservation_station_st_Qk[i],
								instruction_0, instruction_0_A, instruction_0_C, instruction_0_A
							);							
							
							BREAK = 1'b0;
						end
				end
				
				if(instruction_0 != `BEQ & reservation_station_add_slots > 0 & reservation_station_mul_slots > 0 & reservation_station_ld_slots > 0 & reservation_station_st_slots > 0) begin
					pc = pc + 8'b1;	
					if(reorder_buffer_slots > 8'b1 & pc < number_of_instructions_executed) begin
						instruction_1 = instruction_memory[pc][15:12];
								instruction_1_A = instruction_memory[pc][11:8];
								instruction_1_B = instruction_memory[pc][7:4];
								instruction_1_C = instruction_memory[pc][3:0];
						//Se as instrucoes sao de ADD, SUB, BEQ e a estacao de reserva estiver vazia
						if((instruction_1 == `ADD | instruction_1 == `SUB | instruction_1 == `BEQ) & reservation_station_add_slots > 0) begin
							BREAK = 1'b1;
							for(i=0; i<4; i=i+1)
								if(BREAK & !reservation_station_add_busy[i]) begin
									reservation_station_add_busy[i] = 1'b1;
									reservation_station_add_op[i] = instruction_1;
									reservation_station_add_tag[i] = {clock_counter[4:0],pc[5:0]};
									
									populates_the_i_reservation_station_if_there_is_data_dependency(
										reservation_station_add_Vj[i], reservation_station_add_Qj[i], reservation_station_add_Vk[i], reservation_station_add_Qk[i],
										instruction_1, instruction_1_A, instruction_1_B, instruction_1_C
									);						
									if(instruction_1 != `BEQ) //Se a instrucao nao for de desvio preenche com a tag
										register_bank_tag[instruction_1_A] = {clock_counter[4:0],pc[5:0]};
									
									if(instruction_1 == `BEQ
									) begin
										//reservation_station_add_op[i] = `BEQ;
										pc = instruction_1_A;
									end
									
									BREAK = 1'b0;
								end
						end
						else if(instruction_1 == `MUL | instruction_1 == `DIV) begin
							BREAK = 1'b1;
							for(i=0; i<4; i=i+1)
								if(BREAK & !reservation_station_mul_busy[i]) begin
									reservation_station_mul_busy[i] = 1'b1;
									reservation_station_mul_op[i] = instruction_1;
									reservation_station_mul_tag[i] = {clock_counter[4:0],pc[5:0]};
									
									populates_the_i_reservation_station_if_there_is_data_dependency(
										reservation_station_mul_Vj[i], reservation_station_mul_Qj[i], reservation_station_mul_Vk[i], reservation_station_mul_Qk[i],
										instruction_1, instruction_1_A, instruction_1_B, instruction_1_C
									);
									//Define a tag para o registrador de destino
									register_bank_tag[instruction_1_A] = {clock_counter[4:0],pc[5:0]};
									
									BREAK = 1'b0;
								end
						end
						else if(instruction_1 == `LD) begin
							BREAK = 1;
							for(i=0; i<4; i=i+1)
								if(BREAK & !reservation_station_ld_busy[i]) begin
									reservation_station_ld_busy[i] = 1'b1;
									reservation_station_ld_tag[i] = {clock_counter[4:0],pc[5:0]};
									reservation_station_ld_offset[i] = instruction_1_B;
									
									if(register_bank_tag[instruction_1_C] != `TAG_EMPTY)
										reservation_station_ld_Qj[i] = register_bank_tag[instruction_1_C];
									else begin
										reservation_station_ld_Qj[i] = `TAG_EMPTY;
										reservation_station_ld_Vj[i] = register_bank_data[instruction_1_C];
									end
									
									register_bank_tag[instruction_1_A] = {clock_counter[4:0],pc[5:0]};
									
									BREAK = 1'b1;
									k = reorder_buffer_index;			
									while(reorder_buffer_busy[k])
										k = k + 1;
									if(k == 8) k = 0;
									for(j = k; j<8; j=j+1)
										if(BREAK & !reorder_buffer_busy[j]) begin
											reorder_buffer_busy[j] = 1'b1;
											reorder_buffer_op[j] = instruction_1;  //Define aoperacao
											reorder_buffer_have_value[j] = 0;
											reorder_buffer_tag[j] = {clock_counter[4:0],pc[5:0]};
											reorder_buffer_dst[j] = instruction_1_A;
											BREAK = 0;
										end
									BREAK = 0;
								end             
						end
						else if(instruction_1 == `ST) begin
							BREAK = 1'b1;
							for(i=0; i<4; i=i+1)
								if(BREAK & !reservation_station_st_busy[i]) begin
									reservation_station_st_busy[i] = 1'b1;
									reservation_station_st_tag[i] = {clock_counter[4:0],pc[5:0]};
									reservation_station_st_offset[i] = instruction_1_B;
										
									populates_the_i_reservation_station_if_there_is_data_dependency(
										reservation_station_st_Vj[i], reservation_station_st_Qj[i], reservation_station_st_Vk[i], reservation_station_st_Qk[i],
										instruction_1, instruction_1_A, instruction_1_C, instruction_1_A
									);
									
									BREAK = 1'b0;								
								end
						end	
						
						if(instruction_1 != `BEQ) begin
							pc = pc + 1;
						end					
					end
				end
			end
		end
		/* 
		  Se a unidade nao estiver ativa, para cada instrucao valida na sua respectiva estacao de reserva que nao
		   tenha conflito de dados verdadiro com os operandos, preenche a unidade funcional correspondente com o 
		   operando B e C e com o index, valida o busy, e	define o estado inicial e final da operacao e a propria
		   operacao a ser realizada, sem que haja conflito estrutural 
		*/
		if(!unit_add_busy)
			for(i=0; i<4; i=i+1)
				if(!unit_add_busy & reservation_station_add_busy[i])
					if(reservation_station_add_Qj[i] == `TAG_EMPTY & reservation_station_add_Qk[i] == `TAG_EMPTY) begin
						unit_add_busy = 1'b1;
						unit_add_B = reservation_station_add_Vj[i];
						unit_add_C = reservation_station_add_Vk[i];
						unit_add_index = i;
						unit_add_state = 2'b0;
						unit_add_done = 1'b0;
						unit_add_op = reservation_station_add_op[i];
					end
		if(!unit_mul_busy)
			for(i=0; i<4; i=i+1)
				if(!unit_mul_busy & reservation_station_mul_busy[i])
					if(reservation_station_mul_Qj[i] == `TAG_EMPTY & reservation_station_mul_Qk[i] == `TAG_EMPTY) begin
						unit_mul_busy = 1'b1;
						unit_mul_B = reservation_station_mul_Vj[i];
						unit_mul_C = reservation_station_mul_Vk[i];
						unit_mul_index = i;
						unit_mul_state = 2'b0;
						unit_mul_done = 1'b0;
						unit_mul_op = reservation_station_mul_op[i];
					end
		if(!unit_addr_busy)
			for(i=0; i<4; i=i+1)
				if(!unit_addr_busy & reservation_station_st_busy[i])
					if(reservation_station_st_Qj[i] == `TAG_EMPTY & reservation_station_st_Qk[i] == `TAG_EMPTY) begin
						unit_addr_busy = 1'b1;
						unit_addr_B = reservation_station_st_offset[i];
						unit_addr_C = reservation_station_st_Vj[i];
						unit_addr_index = i;
						unit_addr_state = 2'b0;
						unit_addr_done = 1'b0;
						unit_addr_op = `ST;
					end
		if(!unit_addr_busy)
			for(i=0; i<4; i=i+1)
				if(!unit_addr_busy & reservation_station_ld_busy[i])
					if(reservation_station_ld_Qj[i] == `TAG_EMPTY) begin
						unit_addr_busy = 1'b1;
						unit_addr_B = reservation_station_ld_offset[i];
						unit_addr_C = reservation_station_ld_Vj[i];
						unit_addr_index = i;
						unit_addr_state = 2'b0;
						unit_addr_done = 1'b0;
						unit_addr_op = `LD;
					end			
		
		
		//Se existir alguma instrucao valida no buffer de reordenacao
		if(reorder_buffer_have_value[reorder_buffer_index] & reorder_buffer_busy[reorder_buffer_index]) begin
			//Se esta instrucao for de desvio e seu valor for diferente de 0
			if(reorder_buffer_op[reorder_buffer_index] == `BEQ) begin
				if(!reorder_buffer_value[reorder_buffer_index]) begin// & reorder_buffer_value[reorder_buffer_index] == reorder_buffer_tag[reorder_buffer_index]) begin						
					//"Reseta" o buffer de reordenacao
					for(i=0; i<8; i=i+1) begin
						reorder_buffer_busy[i] = 1'b0;
						reorder_buffer_have_value[i] = 1'b0;
					end
					for(i=0; i<4; i=i+1) begin
						reservation_station_add_busy[i] = 1'b0;
						reservation_station_mul_busy[i] = 1'b0;
						reservation_station_ld_busy[i] = 1'b0;
						reservation_station_st_busy[i] = 1'b0;
					end
					pc = reorder_buffer_pc[reorder_buffer_index] + 1;
					reorder_buffer_index = 0;
				end
				else if(reorder_buffer_value[reorder_buffer_index]) begin
					pc = reorder_buffer_dst[reorder_buffer_index];					
				end
			end
			else if(reorder_buffer_op[reorder_buffer_index] == `ST) begin
				mem_busy = 1'b1;
				/* Para cada instrucao na estacao de reserva do STORE, se a tag for a mesma que a tag do buffer de reordenacao no index corrente,
				ocorre a escrita na memoria de dados na posicao do valor enconctrado no buffer de reordencao */ 
				for(i=0; i<4; i=i+1)
					if(reservation_station_st_tag[i] == reorder_buffer_tag[reorder_buffer_index]) begin
						memory_data[reorder_buffer_value[reorder_buffer_index]] = reservation_station_st_Vk[i];
						reservation_station_st_busy[i] = 0;
					end
			end
			/* Se a instrucao nao for de STORE, sera escrito no banco de registradores os dados na posicao do valor enconctrado no buffer de reordencao */
			else begin
					register_bank_data[reorder_buffer_dst[reorder_buffer_index]] = reorder_buffer_value[reorder_buffer_index];
					//register_bank_tag[reorder_buffer_dst[reorder_buffer_index]] = `TAG_EMPTY;
					//Itera sobre todas instrucoes das estacoes de reserva preenchendo possiveis dependencias
					for(i=0; i<4; i=i+1)					
						iterates_the_reorder_buffer_by_filling_in_the_data_dependencies_with_its_values(i);
					
			end
			
			
			if(reorder_buffer_busy[reorder_buffer_index]) begin
				//Avanca o index do buffer de reordenacao para as instrucoes, exceto desvio
				reorder_buffer_busy[reorder_buffer_index] = 0;
				reorder_buffer_index = reorder_buffer_index + 1;
			end
			//CONFIRMACAO DUPLA			
		end
			
		
		//EXECUTA AS INSTRUCOES
		if(unit_addr_busy & !unit_addr_done) begin
			case(unit_addr_state)
				0: unit_addr_state = unit_addr_state + 2'b1;
				1: begin						
					unit_addr_A = unit_addr_B + unit_addr_C;
					unit_addr_done = 1'b1;
				end
			endcase
		end
		if(unit_add_busy & !unit_add_done) begin
			case(unit_add_state)
				0: unit_add_state = unit_add_state + 1;
				1: begin
					unit_add_done = 1;
					if(unit_add_op == `ADD) unit_add_A = unit_add_B + unit_add_C;
					else if(unit_add_op == `SUB) unit_add_A = unit_add_B - unit_add_C;
					else if(unit_add_op == `BEQ & unit_add_B == unit_add_C) unit_add_A = 1;
					else if(unit_add_op == `BEQ & unit_add_B != unit_add_C) unit_add_A = 0;
				end
			endcase
		end
		if(unit_mul_busy & !unit_mul_done) begin
			case(unit_mul_state)
				0, 1, 3, 4, 5: unit_mul_state = unit_mul_state + 4'b1;
				2: begin
					if(unit_mul_op == `MUL) begin 
						unit_mul_A = unit_mul_B * unit_mul_C;
						unit_mul_done = 1'b1;
					end
					else if(unit_mul_op == `DIV) unit_mul_state = unit_mul_state + 4'b1;
				end
				6: begin
					if(unit_mul_op == `DIV) begin
						unit_mul_A = unit_mul_B / unit_mul_C;
						unit_mul_done = 1'b1;
					end
				end
			endcase
		end
		
			//Se alguma das instrucoes ficaram prontas nas suas respectivas unidades
		
		//Para todas as instrucoes validas da estacao de reserva do ADD, se sua tag e igual a tag da estacao de reserva, e escrito o valor da unidade ADD
		if(unit_add_done & unit_add_op == `BEQ) begin
			for(i=0; i<8; i=i+1)
				if(reorder_buffer_busy[i] & reorder_buffer_tag[i] == reservation_station_add_tag[unit_add_index]) begin
					reorder_buffer_value[i] = unit_add_A;
					reorder_buffer_have_value[i] = 1;
				end
			
			reservation_station_add_busy[unit_add_index] = 0;	//Desocupa a estacao de reserva
			unit_add_done = 0;	//Desocupa a unidade	
			unit_add_busy = 0;	//Desocupa a unidade	
		end
		if(unit_add_done | unit_mul_done | unit_addr_done) begin
			//Define a prioridade para multiplicacao
			if(unit_mul_done & !CDB_busy) begin
				writing_values(reservation_station_mul_tag[unit_mul_index], unit_mul_A);
				reservation_station_mul_busy[unit_mul_index] = 1'b0;	//"Limpa" a estacao de reserva
				
				unit_mul_done = 1'b0;		//Desocupa a unidade
				unit_mul_busy = 1'b0;		//Desocupa a unidade				
			end
			else if(unit_add_done & !CDB_busy & unit_add_op != `BEQ & !unit_addr_done) begin				
				writing_values(reservation_station_add_tag[unit_add_index], unit_add_A);				
				reservation_station_add_busy[unit_add_index] = 1'b0;	//"Limpa" a estacao de reserva
				
				unit_add_done = 1'b0;		//Desocupa a unidade
				unit_add_busy = 1'b0;		//Desocupa a unidade				
			end
			else if(unit_addr_done & unit_addr_op == `LD & !CDB_busy & !mem_busy) begin
				mem_busy = 1'b1;			
				writing_values(reservation_station_ld_tag[unit_addr_index], memory_data[unit_addr_A]);
				reservation_station_ld_busy[unit_addr_index] = 1'b0;
				
				unit_addr_done = 1'b0;		//Desocupa a unidade
				unit_addr_busy = 1'b0;		//Desocupa a unidade				
			end
		end
		
		if(unit_addr_done & unit_addr_op == `ST & !mem_busy) begin
			mem_busy = 1;
			
			//Para todas as instrucoes validas da estacao de reserva do STORE, se sua tag e igual a tag da estacao de reserva, e escrito na memoria
			for(i=0; i<8; i=i+1)
				if(reorder_buffer_busy[i] & reorder_buffer_tag[i] == reservation_station_st_tag[unit_addr_index]) begin
					reorder_buffer_value[i] = unit_addr_A;
					reorder_buffer_have_value[i] = 1;
				end
				
			unit_addr_done = 1'b0; //Desocupa a unidade	
			unit_addr_busy = 1'b0;	//Desocupa a unidade	
		end
		
		mem_busy = 0;	//Desocupa o acesso a memoria
		
		if(pc >= number_of_instructions_executed) //Permite definir o numero de instrucoes executadas
			done = 1'b1;
		//Verifica se ainda existe alguma instrucao para ser executada
		for(i=0; i<4; i=i+1)
			if(reservation_station_add_busy[i] | reservation_station_mul_busy[i] | reservation_station_ld_busy[i] | reservation_station_st_busy[i])
				done = 1'b0;
		
		if(done == 1'b0)
			clock_counter = clock_counter + 8'b00001;		
		
	end
	
	initial begin		
		for(i=0; i<64; i=i+1)
			memory_data[i] = 16'b0;
		
		for(i=0; i<16; i=i+1) begin			  
			register_bank_data[i] = i;
			register_bank_tag[i] = `TAG_EMPTY;
		end
		
		instruction_memory[0] = {`ADD, `R1, `R1, `R1};		
		instruction_memory[1] = {`ADD, `R1, `R3, `R1};		
		instruction_memory[2] = {`SUB, `R2, `R5, `R5};
		instruction_memory[3] = {`SUB, `R2, `R2, `R2};		
		instruction_memory[4] = {`SUB, `R4, `R4, `R4};		
		instruction_memory[5] = {`ADD, `R6, `R6, `R6};
		instruction_memory[6] = {`ST, `R0, 4'b0000, `R4};
		instruction_memory[7] = {`LD, `R3, 4'b0000, `R2};
		instruction_memory[8] = {`ST, `R3, 4'b0000, `R2};
		instruction_memory[9] = {`LD, `R2, 4'b0000, `R1};
		instruction_memory[10] = {`LD, `R2, 4'b0000, `R1};
		instruction_memory[11] = {`ST, `R2, 4'b0000, `R3};
		instruction_memory[12] = {`LD, `R0, 4'b0000, `R3};
		instruction_memory[13] = {`ST, `R3, 4'b0000, `R1};
		instruction_memory[14] = {`DIV, `R3, `R3, `R3};
		instruction_memory[15] = {`SUB, `R2, `R2, `R2};
		
		number_of_instructions_executed = 16;
		
		for(i=0; i<4; i=i+1) begin
			reservation_station_add_busy[i] = 1'b0;
			reservation_station_ld_busy[i] = 1'b0;
			reservation_station_mul_busy[i] = 1'b0;
			reservation_station_st_busy[i] = 1'b0;
		end
		
		CDB_busy = 1'b0;
		unit_add_busy = 1'b0;
		unit_mul_busy = 1'b0;
		unit_addr_busy = 1'b0;
		mem_busy = 1'b0;
		unit_add_done = 1'b0;
		unit_mul_done = 1'b0;
		unit_addr_done = 1'b0;
		reorder_buffer_index = 1'b0;
		mem_busy = 0;
		
		for(i=0; i<8; i=i+1) begin
			reorder_buffer_busy[i] = 1'b0;
			reorder_buffer_have_value[i] = 1'b0;
		end
		
		clock_counter = 4'b0000;
		done = 1'b0;
		clock = 8'b0;
		pc = 8'b00000000;
		reset = 1'b0;

	  #128 $stop; 
	end
	
	
	always begin
	   #1 clock = ~clock;
	end
	  
	
	initial begin	   
		$monitor(" -------------------------------------------------------------------------------------------------- \n \
		ROB INDEX = %d PC = %b INSTRUCTION = %b INSTRUCTION = %b REORDER BUFFER SLOTS = %d \n \
		\t ADD STATION: \n \
		\t \t busy \t tag \t \t Vj \t Qj \t \t op \t Vk \t Qk \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t MUL STATION: \n \
		\t \t busy \t tag \t \t Vj \t Qj \t \top \t Vk \t Qk \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t LOAD STATION: \n \
		\t \t busy \t tag \t \t Vj \t Qj \t \t offset \n \
		\t \t %b \t %b \t %d \t %b \t %d \n \
		\t \t %b \t %b \t %d \t %b \t %d \n \
		\t \t %b \t %b \t %d \t %b \t %d \n \
		\t \t %b \t %b \t %d \t %b \t %d \n \
		\t STORE STATION: \n \
		\t \t busy \t tag \t\t Vj \t Qj \t \t off \t Vk \t Qk \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t \t %b \t %b \t %d \t %b \t %b \t %d \t %b \n \
		\t REGISTER BANK: \n \
		\t \t tag \t \t data \t \t tag \t \t data\n \
		\t \t %b \t %d \t \t %b \t %d \n \
		\t \t %b \t %d \t \t %b \t %d \n \
		\t \t %b \t %d \t \t %b \t %d \n \
		\t \t %b \t %d \t \t %b \t %d \n \
		\t MEMORY DATA \n \
		\t \t %d %d %d %d %d %d %d %d \n \
		\t REORDER BUFFER \n \
		\t \t busy \t tag \t \t op \t dst \t value \t have value \t pc \n \
		\t \t %b \t %b \t %b \t %b \t %d \t %d \t \t %b \n \
		\t \t %b \t %b \t %b \t %b \t %d \t %d \t \t %b \n \
		\t \t %b \t %b \t %b \t %b \t %d \t %d \t \t %b \n \
		\t \t %b \t %b \t %b \t %b \t %d \t %d \t \t %b \n \
		\t \t %b \t %b \t %b \t %b \t %d \t %d \t \t %b \n \
		\t \t %b \t %b \t %b \t %b \t %d \t %d \t \t %b \n \
		\t \t %b \t %b \t %b \t %b \t %d \t %d \t \t %b \n \
		\t \t %b \t %b \t %b \t %b \t %d \t %d \t \t %b", reorder_buffer_index, pc, instruction_0, instruction_1, reorder_buffer_slots,
		reservation_station_add_busy[0], reservation_station_add_tag[0], reservation_station_add_Vj[0], reservation_station_add_Qj[0], reservation_station_add_op[0], reservation_station_add_Vk[0], reservation_station_add_Qk[0],
		reservation_station_add_busy[1], reservation_station_add_tag[1], reservation_station_add_Vj[1], reservation_station_add_Qj[1], reservation_station_add_op[1], reservation_station_add_Vk[1], reservation_station_add_Qk[1],
		reservation_station_add_busy[2], reservation_station_add_tag[2], reservation_station_add_Vj[2], reservation_station_add_Qj[2], reservation_station_add_op[2], reservation_station_add_Vk[2], reservation_station_add_Qk[2],
		reservation_station_add_busy[3], reservation_station_add_tag[3], reservation_station_add_Vj[3], reservation_station_add_Qj[3], reservation_station_add_op[3], reservation_station_add_Vk[3], reservation_station_add_Qk[3],
		
		reservation_station_mul_busy[0], reservation_station_mul_tag[0], reservation_station_mul_Vj[0], reservation_station_mul_Qj[0], reservation_station_mul_op[0], reservation_station_mul_Vk[0], reservation_station_mul_Qk[0],
		reservation_station_mul_busy[1], reservation_station_mul_tag[1], reservation_station_mul_Vj[1], reservation_station_mul_Qj[1], reservation_station_mul_op[1], reservation_station_mul_Vk[1], reservation_station_mul_Qk[1],
		reservation_station_mul_busy[2], reservation_station_mul_tag[2], reservation_station_mul_Vj[2], reservation_station_mul_Qj[2], reservation_station_mul_op[2], reservation_station_mul_Vk[2], reservation_station_mul_Qk[2],
		reservation_station_mul_busy[3], reservation_station_mul_tag[3], reservation_station_mul_Vj[3], reservation_station_mul_Qj[3], reservation_station_mul_op[3], reservation_station_mul_Vk[3], reservation_station_mul_Qk[3],
		
		reservation_station_ld_busy[0], reservation_station_ld_tag[0], reservation_station_ld_Vj[0], reservation_station_ld_Qj[0], reservation_station_ld_offset[0],
		reservation_station_ld_busy[1], reservation_station_ld_tag[1], reservation_station_ld_Vj[1], reservation_station_ld_Qj[1], reservation_station_ld_offset[1],
		reservation_station_ld_busy[2], reservation_station_ld_tag[2], reservation_station_ld_Vj[2], reservation_station_ld_Qj[2], reservation_station_ld_offset[2],
		reservation_station_ld_busy[3], reservation_station_ld_tag[3], reservation_station_ld_Vj[3], reservation_station_ld_Qj[3], reservation_station_ld_offset[3],
		
		reservation_station_st_busy[0], reservation_station_st_tag[0], reservation_station_st_Vj[0], reservation_station_st_Qj[0], reservation_station_st_offset[0], reservation_station_st_Vk[0], reservation_station_st_Qk[0],
		reservation_station_st_busy[1], reservation_station_st_tag[1], reservation_station_st_Vj[1], reservation_station_st_Qj[1], reservation_station_st_offset[1], reservation_station_st_Vk[1], reservation_station_st_Qk[1],
		reservation_station_st_busy[2], reservation_station_st_tag[2], reservation_station_st_Vj[2], reservation_station_st_Qj[2], reservation_station_st_offset[2], reservation_station_st_Vk[2], reservation_station_st_Qk[2],
		reservation_station_st_busy[3], reservation_station_st_tag[3], reservation_station_st_Vj[3], reservation_station_st_Qj[3], reservation_station_st_offset[3], reservation_station_st_Vk[3], reservation_station_st_Qk[3],

		register_bank_tag[0], register_bank_data[0], 
		register_bank_tag[1], register_bank_data[1], 
		register_bank_tag[2], register_bank_data[2], 
		register_bank_tag[3], register_bank_data[3], 
		register_bank_tag[4], register_bank_data[4], 
		register_bank_tag[5], register_bank_data[5], 
		register_bank_tag[6], register_bank_data[6], 
		register_bank_tag[7], register_bank_data[7],
		
		memory_data[0], memory_data[1], memory_data[2], memory_data[3], memory_data[4], memory_data[5], memory_data[6], memory_data[7],
		
		reorder_buffer_busy[0], reorder_buffer_tag[0], reorder_buffer_op[0], reorder_buffer_dst[0], reorder_buffer_value[0], reorder_buffer_have_value[0], reorder_buffer_pc[0],
		reorder_buffer_busy[1], reorder_buffer_tag[1], reorder_buffer_op[1], reorder_buffer_dst[1], reorder_buffer_value[1], reorder_buffer_have_value[1], reorder_buffer_pc[1],
		reorder_buffer_busy[2], reorder_buffer_tag[2], reorder_buffer_op[2], reorder_buffer_dst[2], reorder_buffer_value[2], reorder_buffer_have_value[2], reorder_buffer_pc[2],
		reorder_buffer_busy[3], reorder_buffer_tag[3], reorder_buffer_op[3], reorder_buffer_dst[3], reorder_buffer_value[3], reorder_buffer_have_value[3], reorder_buffer_pc[3],
		reorder_buffer_busy[4], reorder_buffer_tag[4], reorder_buffer_op[4], reorder_buffer_dst[4], reorder_buffer_value[4], reorder_buffer_have_value[4], reorder_buffer_pc[4],
		reorder_buffer_busy[5], reorder_buffer_tag[5], reorder_buffer_op[5], reorder_buffer_dst[5], reorder_buffer_value[5], reorder_buffer_have_value[5], reorder_buffer_pc[5],
		reorder_buffer_busy[6], reorder_buffer_tag[6], reorder_buffer_op[6], reorder_buffer_dst[6], reorder_buffer_value[6], reorder_buffer_have_value[6], reorder_buffer_pc[6],
		reorder_buffer_busy[7], reorder_buffer_tag[7], reorder_buffer_op[7], reorder_buffer_dst[7], reorder_buffer_value[7], reorder_buffer_have_value[7], reorder_buffer_pc[7]
		
		);
	
	end	
	
	task iterates_the_reorder_buffer_by_filling_in_the_data_dependencies_with_its_values;
	input integer m;
	begin
		/* Se ha dependencia de dados e a tag na estacao de reserva e igual a tag do buffer de reordenacao, atribui o valor do 
		buffer de reordenacao a estacao de reserva */
		if(reservation_station_add_Qj[m] == reorder_buffer_tag[reorder_buffer_index]) begin
			reservation_station_add_Vj[m] = reorder_buffer_value[reorder_buffer_index];
			reservation_station_add_Qj[m] = `TAG_EMPTY;
		end
		if(reservation_station_add_Qk[m] == reorder_buffer_tag[reorder_buffer_index]) begin
			reservation_station_add_Vk[m] = reorder_buffer_value[reorder_buffer_index];
			reservation_station_add_Qk[m] = `TAG_EMPTY;
		end
		if(reservation_station_mul_Qj[m] == reorder_buffer_tag[reorder_buffer_index]) begin
			reservation_station_mul_Vj[m] = reorder_buffer_value[reorder_buffer_index];
			reservation_station_mul_Qj[m] = `TAG_EMPTY;
		end
		if(reservation_station_mul_Qk[m] == reorder_buffer_tag[reorder_buffer_index]) begin
			reservation_station_mul_Vk[m] = reorder_buffer_value[reorder_buffer_index];
			reservation_station_mul_Qk[m] = `TAG_EMPTY;
		end
		if(reservation_station_ld_Qj[m] == reorder_buffer_tag[reorder_buffer_index]) begin
			reservation_station_ld_Vj[m] = reorder_buffer_value[reorder_buffer_index];
			reservation_station_ld_Qj[m] = `TAG_EMPTY;
		end
		if(reservation_station_st_Qj[m] == reorder_buffer_tag[reorder_buffer_index]) begin
			reservation_station_st_Vj[m] = reorder_buffer_value[reorder_buffer_index];
			reservation_station_st_Qj[m] = `TAG_EMPTY;
		end
		if(reservation_station_st_Qk[m] == reorder_buffer_tag[reorder_buffer_index]) begin
			reservation_station_st_Vk[m] = reorder_buffer_value[reorder_buffer_index];
			reservation_station_st_Qk[m] = `TAG_EMPTY;
		end	
	end
	endtask
	task iterates_the_reservation_station_by_filling_in_the_data_dependencies_with_its_values;
	input n;
	input [8:0] reservation_station_tag;
	input [15:0] A;
		begin
			/* Se ha dependencia de dados e a tag na estacao de reserva e igual a tag do buffer de reordenacao, 
			atribui o valor do buffer de reordenacao a estacao de reserva	*/
			if(reservation_station_add_Qj[n] == reservation_station_tag) begin
				reservation_station_add_Vj[n] = A;
				reservation_station_add_Qj[n] = `TAG_EMPTY;
			end
			if(reservation_station_add_Qk[n] == reservation_station_tag) begin
				reservation_station_add_Vk[n] = A;
				reservation_station_add_Qk[n] = `TAG_EMPTY;
			end
			if(reservation_station_mul_Qj[n] == reservation_station_tag) begin
				reservation_station_mul_Vj[n] = A;
				reservation_station_mul_Qj[n] = `TAG_EMPTY;
			end
			if(reservation_station_mul_Qk[n] == reservation_station_tag) begin
				reservation_station_mul_Vk[n] = A;
				reservation_station_mul_Qk[n] = `TAG_EMPTY;
			end
			if(reservation_station_ld_Qj[n] == reservation_station_tag) begin
				reservation_station_ld_Vj[n] = A;
				reservation_station_ld_Qj[n] = `TAG_EMPTY;
			end
			if(reservation_station_st_Qj[n] == reservation_station_tag) begin
				reservation_station_st_Vj[n] = A;
				reservation_station_st_Qj[n] = `TAG_EMPTY;
			end
			if(reservation_station_st_Qk[n] == reservation_station_tag) begin
				reservation_station_st_Vk[n] = A; 
				reservation_station_st_Qk[n] = `TAG_EMPTY;
			end				
		end
	endtask
	task populates_the_i_reservation_station_if_there_is_data_dependency;
		inout [15:0] reservation_station_Vj;
		inout [10:0] reservation_station_Qj;
		inout [15:0] reservation_station_Vk;
		inout [10:0] reservation_station_Qk;
		input [3:0] instruction;
		input [3:0] register_A;
		input [3:0] register_B;
		input [3:0] register_C;
		begin
			/* Se a tag do banco de registradores for diferente de TAG_EMPTY, existe dependencia de dados verdadeira,
			 entao a tag da instrucao correspondente ao conflito sera escrita na estacao de reserva em Qj e Qk; se nao,
			 sera escrito os proprios dados do banco */
			if(register_bank_tag[register_B] != `TAG_EMPTY) begin
				reservation_station_Qj = register_bank_tag[register_B];
			end
			else begin
				reservation_station_Qj = `TAG_EMPTY;
				reservation_station_Vj = register_bank_data[register_B];
			end
			if(register_bank_tag[register_C] != `TAG_EMPTY) begin
				reservation_station_Qk = register_bank_tag[register_C];
			end
			else begin
				reservation_station_Qk = `TAG_EMPTY;
				reservation_station_Vk = register_bank_data[register_C];
			end
			BREAK = 1;
			
			//Insere os dados da instrucao no buffer de reordenacao
			k = reorder_buffer_index;			
			while(reorder_buffer_busy[k])
				k = k + 1;
			if(k == 8) k = 0;
			for(j = k; j<8; j=j+1)
				if(BREAK & !reorder_buffer_busy[j]) begin
					reorder_buffer_busy[j] = 1'b1;
					reorder_buffer_op[j] = instruction;  //Define aoperacao
					reorder_buffer_have_value[j] = 1'b0;
					reorder_buffer_tag[j] = {clock_counter[4:0],pc[5:0]};
					reorder_buffer_dst[j] = register_A;
					reorder_buffer_pc[j] = pc;
					BREAK = 0;
				end			
		end
	endtask
	task writing_values;
		inout [10:0] reservation_station_tag;
		inout [15:0] A;
		begin
			CDB_busy = 1'b1;
			for(i=0; i<8; i=i+1)
				if(reorder_buffer_busy[i] & reorder_buffer_tag[i] == reservation_station_tag) begin
					reorder_buffer_value[i] = A;
					reorder_buffer_have_value[i] = 1'b1;
				end
			//Percorre todas as estacoes de reserva procurando por dependencias
			BREAK = 1;
			for(i=0; i<4; i=i+1)
				if(BREAK)
					iterates_the_reservation_station_by_filling_in_the_data_dependencies_with_its_values(
						i, reservation_station_tag, A
					);
			CDB_busy =1'b0;		//Desecupa o CDB
		end
	endtask
endmodule

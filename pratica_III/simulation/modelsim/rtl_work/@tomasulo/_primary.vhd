library verilog;
use verilog.vl_types.all;
entity Tomasulo is
    generic(
        RS_SIZE         : integer := 4;
        RB_SIZE         : integer := 8
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of RS_SIZE : constant is 1;
    attribute mti_svvh_generic_type of RB_SIZE : constant is 1;
end Tomasulo;
